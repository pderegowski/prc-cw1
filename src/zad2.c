/*
 * zad2.c
 *
 *  Created on: Nov 13, 2020
 *      Author: root
 */


#include <stdio.h>
#include <stdlib.h>

int main(void){

	int zIntiger;
	char zChar;
	unsigned char zUnsignedChar;
	float zFLoat;
	double zDouble;

	zIntiger=5;
	zChar = 100;
	zUnsignedChar= 101;
	zFLoat=1234567;
	zDouble=12345678912345;

	printf ("Wartosci: \n int = %i \n char = %c \n unsigned char = %c \n float = %.5f \n double = %.15f\n", zIntiger, zChar, zUnsignedChar, zFLoat, zDouble);
	printf("Rozmiary w bajtach: \n -int : %ld \n -char: %ld \n -float %ld \n -double : %ld", sizeof(zIntiger), sizeof(zChar), sizeof(zFLoat), sizeof(zDouble));


	const int zAint = 8;

	return EXIT_SUCCESS;
}



