/*
 * zad3.c
 *
 *  Created on: Nov 13, 2020
 *      Author: root
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int za = 0;
	int zb = 100;

	printf(" Podstawowe zmienne (bez zmian)= \n"
			"za: %i  zb: %i \n", za, zb);

	for(int j = 0; j<20; j++){
		za++;
		zb-=2;
	}
	printf("Integer za zwiekszona dwudziestokrotnie o 1 i Integer zb zmiejszone  dzwudziestokrotnie o 2= \n"
			"za: %i  zb: %i \n", za, zb);

	int i =0;
	while(i<20){
		za+=2;
		zb+=2;
		i++;
	}
	printf("Integer za zwiekszone dzwudziestokrotnie o 2 i to Integer zb wiekszone dzwudziestokrotnie o 2 d: %i  zb: %i \n", za, zb);

	return EXIT_SUCCESS;
}


